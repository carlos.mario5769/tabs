package com.ecci.navegation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController

class RedFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_red, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.navButton1).setOnClickListener {
            //Navegar al fragmento azul
            view.findNavController().navigate(R.id.action_redFragment_to_blueFragment)
        }
        view.findViewById<Button>(R.id.navButton2).setOnClickListener {
            //Navegar al fragmento verde
            view.findNavController().navigate(R.id.action_redFragment_to_greenFragment)
        }
    }
}