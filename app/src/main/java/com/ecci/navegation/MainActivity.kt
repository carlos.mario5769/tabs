package com.ecci.navegation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupNavigation()
    }

    //Configurar el AppBar con la información del navigation
    private fun setupNavigation() {
        val navController = findNavController(R.id.nav_container)
        val appBarConfiguration = AppBarConfiguration(setOf(R.id.blueFragment, R.id.navigation_friend, R.id.greenFragment, R.id.navigation_home, R.id.navigation_profile))
        setupActionBarWithNavController(navController, appBarConfiguration)
        //Configuramos navegador inferior
        val navBottom = findViewById<BottomNavigationView>(R.id.navBottom)
        navBottom.setupWithNavController(navController)
    }
}